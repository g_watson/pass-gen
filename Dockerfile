FROM alpine:latest

COPY pass-gen /opt/pass-gen

EXPOSE 8000

ENTRYPOINT ["/opt/pass-gen"]
