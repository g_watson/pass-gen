package main

import (
	"math/rand"
	"strings"
	"time"
)

type passwordParams struct {
	Length    int  `json:"length"`
	SpecChars bool `json:"specchars"`
	Numbers   bool `json:"numbers"`
}

type passwordResp struct {
	Password string `json:"password"`
}

var (
	letters   = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	specChars = []rune("!@#$%^&*()?-")
	numbers   = []rune("0123456789")
)

func genPass(pp passwordParams) passwordResp {
	// Set password length
	passLength := PASSWORD_MAX_LENGTH

	if pp.Length < PASSWORD_MAX_LENGTH {
		passLength = pp.Length
	}

	genPassword := make([]string, passLength)

	// Set the random seed
	rand.Seed(int64(time.Now().Nanosecond()))

	for i := range genPassword {
		genPassword[i] = getPassValue(pp)
	}

	return passwordResp{Password: strings.Join(genPassword, "")}
}

func getPassValue(pp passwordParams) string {
	passwordValue := ""

	for passwordValue == "" {
		randType := rand.Intn(3)
		switch randType {
		case 0:
			passwordValue = string(letters[rand.Intn(len(letters))])
		case 1:
			if pp.SpecChars {
				passwordValue = string(specChars[rand.Intn(len(specChars))])
			}
		case 2:
			if pp.Numbers {
				passwordValue = string(numbers[rand.Intn(len(numbers))])
			}
		}
	}

	return passwordValue
}
