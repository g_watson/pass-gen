package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/joho/godotenv"

	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	PASSWORD_MAX_LENGTH int = 25
)

func main() {
	setup()

	http.HandleFunc("/genpass", handleGenPass)
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func handleGenPass(rw http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}

	var pp passwordParams
	err = json.Unmarshal(body, &pp)
	if err != nil {
		panic(err)
	}

	// Marshal response
	generatedPass := genPass(pp)
	rw.WriteHeader(http.StatusOK)

	jsonResponse, err := json.Marshal(generatedPass)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	logRequest(pp, req)

	// Send response
	rw.Write([]byte(jsonResponse))
}

func setup() {
	// Load env vars
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Setup using env vars
	b, err := strconv.ParseBool(os.Getenv("LOG_TO_FILE"))
	if err != nil {
		log.Fatal("Error loading LOG_TO_FILE env var: ", err)
	}

	if b {
		log.SetOutput(&lumberjack.Logger{
			Filename:   os.Getenv("LOG_FILE"),
			MaxSize:    20, // megabytes
			MaxBackups: 3,
			MaxAge:     30, //days
		})
	}
}

func logRequest(pp passwordParams, req *http.Request) {
	log.Println(req.RemoteAddr, " ", req.RequestURI, " ", pp.Length, " ", pp.SpecChars, " ", pp.Numbers, " ")
}
